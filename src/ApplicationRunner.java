import io.catalyte.controller.EmployeeController;
import io.catalyte.controller.OfficeController;
import io.catalyte.dao.EmployeeCsvData;
import io.catalyte.dao.EmployeeXmlData;
import io.catalyte.dao.OfficeData;
import io.catalyte.entity.Employee;
import io.catalyte.service.EmployeeServiceImpl;
import io.catalyte.service.OfficeServiceImpl;

public class ApplicationRunner {
    public static void main(String[] args) {

//        EmployeeController employeeController = new EmployeeController();
//        EmployeeServiceImpl employeeServiceImpl = new EmployeeServiceImpl();
//        EmployeeXmlData employeeXmlData = new EmployeeXmlData();
//        EmployeeCsvData employeeCsvData = new EmployeeCsvData();
//
//        employeeController.setEmployeeService(employeeServiceImpl);
//        employeeServiceImpl.setEmployeeDao(employeeCsvData);
//
//        employeeController.start();
//
//        employeeServiceImpl.setEmployeeDao(employeeXmlData);
//
//        employeeController.start();

        OfficeController officeController = new OfficeController();
        OfficeServiceImpl officeServiceImpl = new OfficeServiceImpl();
        OfficeData officeData = new OfficeData();


        officeController.setOfficeService(officeServiceImpl);
        officeServiceImpl.setOfficeDao(officeData);

        officeController.start();
    }
}