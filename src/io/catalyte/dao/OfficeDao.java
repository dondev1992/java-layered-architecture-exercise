package io.catalyte.dao;

import io.catalyte.entity.Office;

import java.util.List;

public interface OfficeDao {
    List<Office> getOffices();
}
