package io.catalyte.controller;

import io.catalyte.dao.EmployeeDao;
import io.catalyte.service.EmployeeService;
import io.catalyte.service.EmployeeServiceImpl;

public class EmployeeController {
    public EmployeeService employeeService;

    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    public void start() {
        employeeService.getEmployees();
    }
}
