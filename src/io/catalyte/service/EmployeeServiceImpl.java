package io.catalyte.service;

import io.catalyte.dao.EmployeeCsvData;
import io.catalyte.dao.EmployeeDao;

public class EmployeeServiceImpl implements EmployeeService{
    public EmployeeDao employeeDao;
    public EmployeeService employeeService;

    public void setEmployeeDao(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    @Override
    public void getEmployees() {
        employeeDao.getEmployees();
    }
}
