package io.catalyte.service;

import io.catalyte.dao.OfficeDao;

public class OfficeServiceImpl implements OfficeService{

    public OfficeDao officeDao;
    public OfficeService officeService;

    public void setOfficeDao(OfficeDao officeDao) {
        this.officeDao = officeDao;
    }

    @Override
    public void getOffices() {
        officeDao.getOffices();
    }
}
